import traceback
import cv2

from lane_detection.image_processing.lane_object import LaneContourHandler
import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image, CameraInfo
from lanelet_msgs.msg import Lanelet,LineString
from cv_bridge import CvBridge
from lane_detection.image_processing.color_filtering import  ColorRange, Color_Filter_Handler
from lane_detection.image_processing.region_of_interest_classes import Roi_Handler
from lane_detection.image_processing.eagelView import EagelView
import numpy as np 

import time
from lane_detection.image_params import image_lanelet

class LaneDetectionHandler(Node):
    def __init__(self, 
                 x_off_set_pixel, 
                 y_off_set_pixel,
                 pixel_to_meter_x, 
                 pixel_to_meter_y,
                 image_show, logger_show,
                 kernel_size, publish_data, 
                 subscription_data, call_back_information,
                 color_ranges = [ColorRange(0, 0, 100, 255, 255, 255)]) -> None:
        self.logger_show=logger_show
        self.color_ranges = color_ranges
        self.call_back_information = call_back_information
        self.color_handler = Color_Filter_Handler(color_ranges,(5,5),(1,1),(5,5))
        self.roi = Roi_Handler()
        self.image_show=image_show
        self.eagel_view=None
        
        super().__init__('subscriber_and_publisher')

        self.publisher = self.create_publisher(
             publish_data['msg_type'],
             publish_data['topic'],
             publish_data['queue_size']) 
 

        self.camera_info_subscription = self.create_subscription(
            CameraInfo,
            "/citicar/camera/camera_info",  
            self.callback_camera_info,
            subscription_data['queue_size']
            )
        
       
        self.subscription = self.create_subscription(
            subscription_data['msg_type'],
            subscription_data['topic'],
            self.callback_get_lane,
            subscription_data['queue_size']
            )
      
    def draw_offsets(self,img):
        i = 0
        while (i * 200 ) < img.shape[0]:
            horizontal_line_y = (int)(i * 200)
            cv2.line(
                img,
                (0,horizontal_line_y),
                (img.shape[1],horizontal_line_y),
                (255,0,0),
                3
                )
            
            cv2.putText(
                img,
                str((int)(horizontal_line_y))  + "p", 
                (10,horizontal_line_y+1),
                1,
                4,
                (255,255,255),
                2,
                cv2.LINE_AA
                )
            i = i + 0.5
        
        i = 0
        while (i * 200 ) < img.shape[1]: 
            horizontal_line_x = (int)(i * 200 )
            
            cv2.line(img,
                    (horizontal_line_x,0),
                    (horizontal_line_x,img.shape[0]),
                    (255,0,0),
                    3
                    )
            
            cv2.putText(img,
                        str((int)(horizontal_line_x)) + "p",
                        (horizontal_line_x,50),
                        1,
                        3,
                        (255,255,255),
                        2,
                        cv2.LINE_AA
                        )
            i = i + 0.5  
    #Quelle 
    # Xin Yang and Houyu Yu "Lane Line Detection Based on Dynamic ROI and Adaptive Thresholding Segmentation."
    # https://dl.acm.org/doi/pdf/10.1145/3573428.3573524
    def adaptive_threshholding_segmentation(self,binary_image):
        # Calculate the number of image rows
        num_rows = binary_image.shape[0]

        # Define thresholds
        vertical_distance_threshold = (1/500) * num_rows
        #width_threshold_percentage = 0.8
        width_threshold_percentage = 0.2

        # Find connected components in the binary image
        _, connected_components = cv2.connectedComponents(binary_image)

        # Iterate through the extracted connected components
        filtered_regions = []
        for label in range(1, np.max(connected_components) + 1):
            region_mask = (connected_components == label)
            y_coordinates = np.nonzero(region_mask)[0]
            y_max = np.max(y_coordinates)
            y_min = np.min(y_coordinates)
            y_distance = y_max - y_min

            # Check the vertical distance condition
            if y_distance > vertical_distance_threshold:
                # Calculate the pixel count in each row
                row_pixel_counts = np.sum(region_mask, axis=1)
                num_non_lane_rows = np.sum(row_pixel_counts > width_threshold_percentage * binary_image.shape[1])

                # Check the width condition
                if num_non_lane_rows <= width_threshold_percentage * num_rows:
                    filtered_regions.append(region_mask)

        # Create an empty output image
        filtered_image = np.zeros_like(binary_image)

        # Set the filtered regions in the output image to 255
        for idx, region_mask in enumerate(filtered_regions):
            filtered_image[region_mask] = 255

        return filtered_image
    



    def start_obj(self,thresh,image):
        self.lan_con_handler = LaneContourHandler(thresh=thresh)
        #print(" finish handeling")
        for con in self.lan_con_handler.middle_line_countours:
            cv2.drawContours(image, [con.contour], 0, (0, 0, 255), 2)
        self.draw_offsets(image) 
        return image



    def get_image_from_thresh(self,thresh):
        return cv2.merge((thresh,thresh,thresh))


    def start(self,image):
        start_time = time.time() 
        mask = self.roi.get_mask(image)
        # Apply bitwise AND operation between the mask and the gray image
        masked_image = cv2.bitwise_and(image, image, mask=mask)
        mask = self.color_handler.filter_color(masked_image)
        mask = self.color_handler.morphology_filter()
        masked_image = cv2.bitwise_and(image, image, mask=mask)
        thresh = self.color_handler.filter_dilate()
        #thresh = self.adaptive_threshholding_segmentation(thresh)
        

        image = self.start_obj(thresh,image)
           
        elapsed_time = time.time() - start_time
        fps = 1/elapsed_time
        fps_text = f"FPS: {fps:.2f}"
        cv2.putText(image,fps_text,(10,50),1,4,255,2)
        if self.image_show:
            stackedImage = np.hstack((cv2.cvtColor(image, cv2.COLOR_BGR2RGB),self.get_image_from_thresh(thresh)))
            stackedImage = np.hstack((stackedImage,masked_image))
            display_image_half_size(stackedImage)
            #self.start_with_Hough_Lines(thresh,image)


    def callback_camera_info(self,msg):
        # Extrahieren Sie die benötigten Informationen aus der CameraInfo-Nachricht
        self.eagel_view=EagelView(
             msg.width,msg.height,msg.k,msg.d,msg.distortion_model
        )

        self.width = msg.width
        self.height = msg.height
        self.distortion_model = msg.distortion_model
        K = msg.k  # Die Kalibrierungsmatrix (intrinsische Parameter)
        D = msg.d  # Die Verzerrungskoeffizienten

        # Verarbeiten Sie die Kamerainformationen nach Bedarf
        # Hier können Sie die extrahierten Informationen für Ihre Anwendung verwenden
        # Zum Beispiel können Sie die Kamerakalibrierungsmatrix (intrinsische Parameter) verwenden, um 3D-Koordinaten aus Bildpunkten zu rekonstruieren.
        # Beispiel: Ausgabe der Informationen
        if self.logger_show:
            self.get_logger().info(f"\nWidth: {self.width}\nHeight: {self.height}\nDistortion Model: {self.distortion_model}\n"\
                                   f"Intrinsics (K): {K}\nDistortion Coefficients (D): {D}\n")    


    def callback_get_lane(self,msg):
        bridge = CvBridge()
        try:

            # convert msg into image
            cv_image = bridge.imgmsg_to_cv2(msg, desired_encoding='passthrough')
            #cv_image = self.eagel_view.transform_image(cv_image)
            cv_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)            
            self.start(cv_image)
            #publisher entry point
            middle=self.lan_con_handler.left_line
            right=self.lan_con_handler.righ_line

            publishData = Lanelet
            publishData.middle_lane = LineString
            publishData.right_lane= LineString
            #self.publisher.publish(publishData)
            
        except Exception as e:
            self.get_logger().info(f"exception in image_callback in the Subscriber:\n{e}")
            traceback.print_exc()
            

    def show_display(self,cv_image):

        if (self.ImageProcessor.mask is not None and
             self.call_back_information['show_option'] == "mask"):
                display_image_half_size(
                     cv2.addWeighted(self.ImageProcessor.mask,0.5,self.ImageProcessor.img,0.7,0))

        elif (self.ImageProcessor.mask is not None 
              and self.call_back_information['show_option'] == "mask_only"):
                display_image_half_size(self.ImageProcessor.mask)
        elif (self.ImageProcessor.bin_image is not None 
              and self.call_back_information['show_option'] == "bin_image"):
                display_image_half_size(self.ImageProcessor.bin_image)      
        elif (self.ImageProcessor.bin_image is not None 
              and self.call_back_information['show_option'] == "bin_normal"):
            display_image_half_size(
                 cv2.addWeighted(self.ImageProcessor.img,0.5,self.ImageProcessor.bin_image,0.7,0))

        elif (self.ImageProcessor.img is not None 
              and self.call_back_information['show_option'] == "normal"):
                display_image_half_size(
                     cv2.addWeighted(self.ImageProcessor.img,0.5,cv_image,0.7,0))
        elif self.call_back_information['show_option'] == "clean":
            display_image_half_size(cv_image)
        else:
            if self.ImageProcessor.img is not None:
                display_image_half_size(self.ImageProcessor.img)
            else:    
                display_image_half_size(cv_image)
            


def display_image_half_size(image):
    # Get the original image dimensions
    height, width = image.shape[:2]

    # Calculate the new dimensions for resizing
    new_width = int(width / 2)
    new_height = int(height / 2)

    # Resize the image to half its size
    resized_image = cv2.resize(image, (new_width, new_height))

    # Display the resized image
    cv2.imshow("Resized Image", resized_image)
    cv2.waitKey(1)


def main(args=None):
    rclpy.init()
    node=rclpy.create_node('PozyxPublisher')
    param_listener=image_lanelet.ParamListener(node)
    params = param_listener.get_params()
    publisher_data = {
        'msg_type': Lanelet,                      # The message type for the publisher
        'topic': params.topic_publish,                  # The topic to publish to
        'queue_size': params.queue_size_publish,        # The size of the message queue
    }

    subscription_data = {
        'msg_type': Image,                        # The message type for the subscription
        'topic': params.topic_subscribe,            # The topic to subscribe to
        'queue_size': params.queue_size_subscribe,  # The size of the message queue
    }

    call_back_information = {
        'frame_id' : params.frame_id,
        'show_option' : params.show,
    }
    lane_let = LaneDetectionHandler(
        params.x_off_set_pixel,
        params.y_off_set_pixel,
        params.pixel_to_meter_x,
        params.pixel_to_meter_y,
        params.image_show,
        params.logger_show,
        (params.kernel,params.kernel),
        publish_data=publisher_data,
        subscription_data=subscription_data,
        call_back_information=call_back_information
    )
    rclpy.spin(lane_let)
    lane_let.destroy_node()

if __name__ == '__main__':
    main()