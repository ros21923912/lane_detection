import cv2
import numpy as np


class FilterMiddleLines:
    def __init__(self,  
                min_contour_length=45,
                max_contour_length=500,
                max_aspect_ratio=20,
                max_line_fit_error=40,
                filter_standard_abweichung=False,
                filter_box_aspect_ratio=False,
                min_area_aspect_ratio=0.08,
                max_area_aspect_ratio=0.5,
                max_width=200
                ): 
        self.min_contour_length = min_contour_length
        self.max_contour_length = max_contour_length
        self.max_aspect_ratio = max_aspect_ratio
        self.max_line_fit_error = max_line_fit_error
        self.filter_standard_abweichung = filter_standard_abweichung
        self.filter_box_aspect_ratio = filter_box_aspect_ratio
        self.min_area_aspect_ratio = min_area_aspect_ratio
        self.max_area_aspect_ratio = max_area_aspect_ratio
        self.max_width = max_width


    def is_middle_lane(self,laneCountour):
        if laneCountour.contour_length < self.min_contour_length: 
            return False
        if laneCountour.contour_length > self.max_contour_length:
            return False

        if laneCountour.contour_area < 1:
            return False
        
        if (self.filter_standard_abweichung):
            if (laneCountour.y_std > 20 and laneCountour.x_std > 20):
                return False
            
            if (np.abs(laneCountour.angle - np.pi / 2) > 1.5 and LaneContourHandler.x_std > 20):
                return False

            if (np.abs(laneCountour.ngle - np.pi / 2) > 1 and laneCountour.x_std > 30):
                return False

            if (np.abs(laneCountour.angle - np.pi / 2) > 2 and laneCountour.x_std > 10):
                return False
        
        if laneCountour.width > self.max_width:
            return False
        
        if self.filter_box_aspect_ratio:
            if laneCountour.b_box_aspect_ratio > self.max_aspect_ratio or laneCountour.b_box_aspect_ratio_2 > self.max_line_fit_errormax_aspect_ratio: 
                return False

            if self.filter_box_aspect_ratio:
                if self.b_box_aspect_ratio > self.max_aspect_ratio or laneCountour.b_box_aspect_ratio_2 > self.max_aspect_ratio: 
                    return False             
            
            if self.area_aspect_ratio > laneCountour.max_area_aspect_ratio or self.area_aspect_ratio < laneCountour.min_area_aspect_ratio:
                return False

        if laneCountour.line_fit_error > self.max_line_fit_error:
            return False 
        return True



class LaneContourHandler:
    def __init__(self, 
                 thresh,
                 filter_middle_lines=FilterMiddleLines(),
                 max_line_fit_error=40,
                 max_angle=0,
                 max_width=0,
                 min_width=0,
                 min_length=0,
                 max_length=0,
                 use_Hough_lines=True
                 ):
        self.thresh = thresh
        self.filter_middle_lines = filter_middle_lines
        self.prefiltered_lane_countours = []
        self.possible_lines = []
        self.other_countours = []
        edges = cv2.Canny(thresh,50,200) 
        contours, hierarchy = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        self.middle_line_countours = []
        self.left_line_countours = []
        self.right_line_countours = []

        self.left_line = []
        self.righ_line = []

        for countour in contours:
            obj = LaneCountour(countour)
            if obj.cx == 0 and obj.cy == 0:
                continue

            if(max_width!=0):
                if max_width < obj.width:
                    self.possible_lines.append(obj)
                    continue
            if(max_line_fit_error!=0):
                if max_line_fit_error < obj.line_fit_error:
                    self.possible_lines.append(obj)
                    continue
            if(max_angle!=0):
                if max_angle < obj.angle * (180.0 / np.pi):
                    self.other_countours.append(obj)
                    continue
            if(min_width!=0):
                if min_width > obj.width:
                    self.other_countours.append(obj)
                    continue    
            if(min_length!=0):
                if min_length > obj.length:
                    self.other_countours.append(obj)
                    continue        
            self.prefiltered_lane_countours.append(obj)
        
        
        # source for use Hough Lines from Chatgpt
        if (use_Hough_lines):

            new_line_objects = []
            # Assuming you have a list of contours in 'contours'
            for countour_item in self.possible_lines:
               
               
                height, width = self.thresh.shape
                mask = np.zeros((height, width), dtype=np.uint8)
                cv2.drawContours(mask, [countour_item.contour], -1, 255, thickness=cv2.FILLED)
                
                 # Use Hough Line Transform to detect lines within the binary image
                lines = cv2.HoughLinesP(mask, 1, np.pi / 180, threshold=50, minLineLength=10, maxLineGap=5)


                if lines is not None:
                    for line in lines:
                        x1, y1, x2, y2 = line[0]
                        # Create a new contour object (LaneCountour) for each detected line
                        obj = LaneCountour(np.array([[x1, y1], [x2, y2]]))  # Create a 2-point contour
                        # Now 'obj' represents an individual line segment 
                        new_line_objects.append(obj)
            
            self.possible_lines = new_line_objects

        self.find_middle_lane_elements()
    
    # Go through all pre filtered contours and find out if they are middle lane elements
    def find_middle_lane_elements(self):
        i = 0
        while i < len(self.prefiltered_lane_countours):
            #print(i)
            if self.filter_middle_lines.is_middle_lane(self.prefiltered_lane_countours[i]):
                self.middle_line_countours.append(self.prefiltered_lane_countours.pop(i))
            else :
                i = i + 1
        




    def process_contours(self):
        # Sortieren Sie die Liste der ContourObjects nach Konturlänge.
        sorted_contours = sorted(self.contour_objects, key=lambda contour_obj: contour_obj.contour_length)

        for contour_obj in sorted_contours:
            if contour_obj.is_valid_contour():
                position = contour_obj.cx, contour_obj.cy
                print(f"Valid Contour Position: {position}, Contour Length: {contour_obj.contour_length}")






class LaneCountour:
    def __init__(self, contour):
        self.contour = contour
        self.contour_length = cv2.arcLength(self.contour, closed=True)
        self.contour_area = cv2.contourArea(self.contour)
    
        # Berechnung des Schwerpunkts
        M = cv2.moments(self.contour)
        if M["m00"] != 0:
            self.cx = int(M["m10"] / M["m00"])
            self.cy = int(M["m01"] / M["m00"])
        else:
             # the contour area is zero
            self.cx = self.cy = 0
            return 


        #M['mu11'] is a moment representing the product of the differences between the x and y coordinates of the contour points
        # Calculate the angle of the contour in radioan
        #M['mu20'] - M['mu02'] is another moment representing the second-order central moments of the contour.
        self.angle = 0.5 * np.arctan2(2 * M['mu11'], M['mu20'] - M['mu02'])

        #print(f"Angle: {angle:.2f} radians")

        # Berechnung der Bounding Box
        x, y, w, h = cv2.boundingRect(self.contour)
        self.b_box_aspect_ratio = w / float(h)
        self.b_box_aspect_ratio_2 = h / float(w)

        # Berechnung von Aspect Ratio basierend auf Konturlänge und Fläche
        self.area_aspect_ratio = self.contour_length / self.contour_area
        self.width = self.contour_length * self.area_aspect_ratio


        # contour = Numpy array in sjape (n,1,2)
        # n = number of points. 1 number of countour => mostly only one
        # reshape -1 => reshape first dimension
        # automaticly determente the size of dimension 
        # the 2 => because x and y 
        reshaped_contour = contour.reshape(-1,2)
        # standard abweichung
        self.x_std = np.std(reshaped_contour[:, 0])
        self.y_std = np.std(reshaped_contour[:, 1])

        # Calculate the angle between the x and y coordinates of the contour points.
        self.deviation_angle = np.arctan2(self.x_std, self.y_std)

        contour_points = contour[:, 0, :]
        rows, cols = contour_points[:, 1], contour_points[:, 0]
        line = np.polyfit(rows, cols, 1)
        self.line_fit_error = np.sum(np.abs(cols - np.polyval(line, rows))) / len(rows)



