import math

import cv2
import rclpy
import tf2_ros
import numpy as np
from geometry_msgs.msg import Twist

class EagelView:
    def __init__(
            self,
            height,width,
            calibration_matrix,
            distortion_matrix,distortion_model
            ) -> None:
        self.height=height
        self.width=width
        self.calibration_matrix=np.float32([[0, self.height], [1407, self.height], [0, 0], [self.width, 0]])
        self.distortion_matrix=np.float32([[569, self.height], [711, self.height], [0, 0], [self.width, 0]])
        self.distortion_model=distortion_model
        """
        self.node=node
        self.link_name=link_name
        self.origin_link=origin_link
        self.buffer=tf2_ros.buffer.Buffer
        self.transformListener=tf2_ros.transform_listener.TransformListener
        """

    """
    def get_camera_left_z(self,node,buffer):
        return self.transformListener.lookupTransform(
            self.link_name, self.origin_link, rclpy.Time(0)
            ).translation.z
    """
    
    def transform_image(self,image):
        return cv2.warpPerspective(
            cv2.warpPerspective(
                image[450:(450+self.height),0:self.width], 
                cv2.getPerspectiveTransform(self.calibration_matrix, self.distortion_matrix), 
                (self.width, self.height)
                ), 
                cv2.getPerspectiveTransform(
                    self.distortion_matrix, self.calibration_matrix), 
                (self.width, self.height)
            ) # Inverse transformation
"""
def main(args=None):
    rclpy.init(args=args)
    node=rclpy.create_node('eagleDummy')
    eagle=eagelView(node)
    while rclpy.ok:
        print(eagle.get_camera_left_z)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
"""