import argparse
import math
import sys
import cv2
import time
from geometry_msgs.msg import Pose,PoseArray
import numpy as np 


def imageToRgb(img_bgr):
    img_rgb = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB)
    return img_rgb

# Convert BGR image to HSV
def imageToHSV(img_brg):
    hsv_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2HSV)
    return hsv_image

# In this code, cv2.moments() is used to calculate the moments of each contour. 
def findPoint(contour):
    # Calculate the moments of the contour
    moments = cv2.moments(contour)
    if moments["m00"] != 0: 
        cx = int(moments["m10"] / moments["m00"])
        cy = int(moments["m01"] / moments["m00"])
        # This gives us a weighted average of the y-coordinates of the pixels within the contour.
        return [cx,cy]
    return [-1,-1]        

def getPose(x, y, yaw_euler_angle_radians):
    #  Create a Pose
    pose = Pose()
    pose.position.x = y
    pose.position.y = x
    pose.position.z = 0.37  
    [qx, qy, qz, qw] = get_quaternion_from_euler(0,3.14159265359 ,yaw_euler_angle_radians) 
    pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w = qx, qy, qz, qw
    return pose

# This program converts Euler angles to a quaternion.
# Author: AutomaticAddison.com
def get_quaternion_from_euler(roll, pitch, yaw):
    qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) \
         - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
    qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) \
         + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
    qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) \
         - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
    qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) \
         + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
    return [qx, qy, qz, qw]

class Lane_Detector:
    def __init__(self, 
    x_off_set, y_off_set,
    pixel_to_meter_x ,
    pixel_to_meter_y,show,
    roi_handler = None,
    kernel_size=(15,15),
    ):
        self.orginal_img = None
        self.kernel_size = kernel_size
        self.img = None
        self.mask = None
        self.pose_array = None
        self.width = None
        self.heigth = None
        self.x_off_set_pixel = x_off_set
        self.y_off_set_pixel = y_off_set
        self.pixel_to_meter_x = pixel_to_meter_x
        self.pixel_to_meter_y = pixel_to_meter_y
        self.show = show
        self.bin_image = None
        self.roi_handler = None
        self.first_start = True

    def start(self,image):

        if self.first_start:
            self.height, self.width = image.shape[:2]

        self.orginal_img = image
        self.img = imageToRgb(self.orginal_img)
        self.mask = None

    
        
    def filter_from_background(self,img,color_ranges):
        self.orginal_img = img
        self.img = imageToHSV(self.orginal_img)
        self.mask = None
        for color_range in color_ranges:
            low = np.array(
                [color_range.red_down,
                 color_range.green_down,
                 color_range.blue_down]
                )
            up = np.array(
                [color_range.red_up,
                 color_range.green_up,
                 color_range.blue_up]
                )
            mask_1 = cv2.inRange(self.img,low,up)
            self.kernel_filter()
            if self.mask is None:
                self.mask = mask_1
            else:
                self.mask = cv2.bitwise_or(self.mask, mask_1)




    def put_filter_over_image(self):
        result = cv2.bitwise_and(self.img, self.img, mask=self.mask)


    def kernel_filter(self):
        # Creating kernel
        kernel = np.ones(self.kernel_size, np.uint8)

        # Apply closing operation to the mask
        self.mask = cv2.morphologyEx(self.mask, cv2.MORPH_CLOSE, kernel)




    def color_range(self,img,color_ranges):
        self.orginal_img = img
        self.img = imageToRgb(self.orginal_img)
        self.mask = None
        for color_range in color_ranges:
            low = np.array(
                [color_range.red_down,
                 color_range.green_down,
                 color_range.blue_down]
                )
            up = np.array(
                [color_range.red_up,
                 color_range.green_up,
                 color_range.blue_up]
                )
            mask_1 = cv2.inRange(self.img,low,up)
            if self.mask is None:
                self.mask = mask_1
            else:
                self.mask = cv2.bitwise_or(self.mask, mask_1)
        return self.process_image()


    def check_if_circle(self, contour):
        # Get the minimum enclosing circle for the contour
        (x, y), radius = cv2.minEnclosingCircle(contour)

        # Calculate the area of the contour and the area of the circle
        contour_area = cv2.contourArea(contour)
        circle_area = np.pi * radius**2

        # Calculate the circularity, which is the ratio of contour area to circle area
        circularity = contour_area / circle_area

        # Set a threshold to determine if it's a circle
        circularity_threshold = 0.7  
        # Check if the circularity is close to 1 (a perfect circle)
        if circularity >= circularity_threshold:
            return True
        else:
            return False             


    def round_to_next_multiply(self,angle,factor):
        parameter = int(360/factor)
        return round(angle / parameter) * parameter

    def process_image(self):
        # creating an empty PoseArray message
        self.pose_array = PoseArray()
        self.mask = cv2.cvtColor(self.mask, cv2.COLOR_RGB2BGR) 
        # Convert the image to grayscale
        self.gray = cv2.cvtColor(self.mask, cv2.COLOR_BGR2GRAY) 
        
        # Load the image and convert it to grayscale
        # Apply thresholding to create a binary image
        _, thresh = cv2.threshold(self.gray, 0, 255, cv2.THRESH_BINARY)

        # Creating kernel
        kernel = np.ones(self.kernel_size, np.uint8)
        thresh = cv2.erode(thresh, kernel)
        thresh = cv2.dilate(thresh, kernel)
        if "bin" in self.show:
            self.bin_image = cv2.merge((thresh, thresh, thresh))

        # Find the contours of the objects in the binary image
        contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        if len(contours) == 0:
            return
       
        longest_contour = None
        # Calculate the length of each contour and sort them in descending order
        for contour in contours:
            angle = 0
            isCircle = False
            #calculate Point 
            point = findPoint(contour)
            if (point[0]== -1):
                break
            length = cv2.arcLength(contour, True)
              # Calculate the length of the contour
            
            # Approximate the contour with a polygon
            epsilon = 0.01 * length
            # The epsilon value used in cv2.approxPolyDP() is a parameter that controls 
            # the maximum distance between the original curve and its approximation. 
            # A smaller value of epsilon will result in a more accurate approximation,
            #  but will also lead to more vertices in the resulting polygon.
            approx = cv2.approxPolyDP(contour, epsilon, True)

            if not self.check_if_circle(contour) :
              
                # Find the longest side of the polygon
                longest_side_length = 0
                start_index = 0
                smallest_side_length = sys.maxsize

                for i in range(len(approx)):
                    side_length = np.linalg.norm(approx[i] - approx[(i + 1) % len(approx)])
                    if side_length > longest_side_length:
                        longest_side_length = side_length
                        start_index = i        
                    if side_length < smallest_side_length:
                        smallest_side_length = side_length
                
                if (smallest_side_length * 2  < longest_side_length):
                    
                    #   Extract the longest contour
                    longest_contour = approx[start_index: start_index + 2]
                    
                    # Fit a line to the longest contour
                    [vx, vy, x, y] = cv2.fitLine(
                        longest_contour,
                        cv2.DIST_L2, 
                        0, 
                        0.01, 
                        0.01)

                    # Calculate the angle of the line
                    angle = np.arctan2(vy, vx) * 180 / np.pi
                
                    angle_radians = math.radians(
                        self.round_to_next_multiply(int(angle),16))
                else:
                    angle_radians = 0
            else:
                angle_radians = 0
                isCircle = True

            x2 = int(point[0] + length * math.cos(angle_radians))
            y2 = int(point[1] + length * math.sin(angle_radians))


            # Draw the line on the image
            color = (0, 255, 0)
            thickness = 10
            if "normal" in self.show:   
                cv2.circle(self.img,(point[0], point[1]),5, color, thickness)
                cv2.line(self.img, (point[0], point[1]), (x2, y2), color, thickness)
            if "mask" in self.show:    
                cv2.circle(self.mask,(point[0], point[1]),5, color, thickness)
                cv2.line(self.mask, (point[0], point[1]), (x2, y2), color, thickness) 
           
            x_in_meter = (float(point[0])-self.x_off_set_pixel)/self.pixel_to_meter_x
            y_in_meter = (float(point[1])-self.y_off_set_pixel)/self.pixel_to_meter_y

            pose = getPose(x_in_meter,y_in_meter, angle_radians)
            pos_str = "pos={x=%.2f,y=%.2f,angle=%d,isCircle=%s}" % (x_in_meter, 
                                                                    y_in_meter,
                                                                    angle,isCircle)
            
            if "normal" in self.show:   
                cv2.putText(
                    self.img,pos_str, 
                    (point[0], point[1]),
                    1,
                    2,
                    (0,0,255),
                    2,
                    cv2.LINE_AA
                    )
            # add the new Pose object to the PoseArray list
            self.pose_array.poses.append(pose)
        if "normal" in self.show:    
            self.draw_offsets()
        return None
    