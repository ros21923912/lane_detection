import cv2
import numpy as np


class ColorRange:
    def __init__(self, hue_down, sat_down, val_down, hue_up, sat_up, val_up):
        self.hue_down = hue_down
        self.sat_down = sat_down
        self.val_down = val_down
        self.hue_up = hue_up
        self.sat_up = sat_up
        self.val_up = val_up


class Color_Filter_Handler:
    def __init__(self, color_ranges,kernel_fill_size=(0,0),kernel_erode_size=(3,3) ,kernel_size=(9,9)):
        self.mask = None 
        self.color_ranges = color_ranges
        self.kernel_fill_size = kernel_fill_size
        self.kernel_erode_size = kernel_erode_size
        self.kernel_size = kernel_size

    def imageToRgb(img_bgr):
        img_rgb = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB)
        return img_rgb

    # Convert BGR image to HSV
    def image_to_hsv(self,img_brg):
        hsv_image = cv2.cvtColor(img_brg, cv2.COLOR_BGR2HSV)
        return hsv_image

    def morphology_filter(self):
        # Creating kernel
        kernel = np.ones(self.kernel_size, np.uint8)
        # Apply closing operation to the mask
        self.mask =  cv2.morphologyEx(self.mask, cv2.MORPH_CLOSE, kernel)
        return self.mask

    def filter_color(self, img):
        img_hsv = self.image_to_hsv(img)
        self.mask = None
        for color_range in self.color_ranges:
            low = np.array([color_range.hue_down, color_range.sat_down, color_range.val_down])
            up = np.array([color_range.hue_up, color_range.sat_up, color_range.val_up])
            mask_1 = cv2.inRange(img_hsv, low, up)
            if self.mask is None:
                self.mask = mask_1
            else:
                self.mask = cv2.bitwise_or(self.mask, mask_1)  
        return self.mask



    def filter_dilate(self):
        _, thresh = cv2.threshold(self.mask, 1, 255, cv2.THRESH_BINARY)
        
        kernel = np.ones(self.kernel_erode_size, np.uint8)
        thresh = cv2.erode(thresh, kernel)

        kernel = np.ones(self.kernel_fill_size, np.uint8)
        # Dilation is a morphological operation that expands the white regions in a binary image while filling gaps.
        thresh = cv2.dilate(thresh, kernel)
       

        return thresh


    def get_tresh_image(self,thresh):
        return cv2.merge((thresh, thresh, thresh))
         