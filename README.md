# Lane Detection



### Package.xml
The package.xml contains basic information about the ROS2 package, which is broken down as follows:
- xml version: contains the version of the xml format used.
- xml-model: the link given here refers to corresponding ros2 specifications on how package.xml files should be designed. The schema type is a general xml schema type
- format: This indicates which ros2 package format was used for the creation. Normally format 3 is used.
- Name: Name of the ros2 package.
- Version: Here you can change the version number depending on the changes, should a package be updated. 
- description: Can be used to specify a basic abstraction of the package's function.
- maintainer: The guy who takes care of maintenance of the package.
- License: We use Apache-2.0 to ensure free use of the package.
- Dependencies:
    - generate_parameter_library: This library allows us to include declarative parameters, and also ensures the connection to ros2 with the help of codegen. This is not a standard ros2 package, and must therefore be installed later.
'''
sudo apt install ros-humble-generate-parameter-library-py
'''
    - cv2: Is included here to make image processing as simple as possible. Has a good interface for ros2
    - math: Is included to ensure complex calculations in eagle view.
    - tf2_ros: enables the integration of different transformation data. Used here to determine the z coordinate of the camera.
    - rclpy: Basic ros2 python library, without which a ros2 node cannot be built or started in python.
    - lanelet_msgs: Message type, which transmits a header and two point lists.
    - numpy: This is useful for fast and complex calculations in Python.
    - argparse: Parser for command line options. is used for debugging.
    - sensor_msgs: This is used to integrate the image and cameraInfo messages.
    - geometry_msgs: Used for the inclusion of Point Messages, which transmits Lanelet_msgs Point Lists.
    - cv_bridge: Used for multiple image to cv2 conversions.
    - sys: We address very systemic processes, which means we have to get information directly from the system.
    - abc: Allows us to use abstract classes and methods. Used in region_of_interests_classes.py and region_of_interests.py to implement abstract methods.

#### buildtype
determines under which conditions the build tool builds the package. Here, ament_python was chosen to build a python package.

### setup.py
Used in python packages to define the execution points of the nodes and to create specific modules. Used here to create a module using the parameter file to bind it properly. The generate_parameter_py dependency is also included here.

### config
This is where the files with the parameters for the individual nodes are normally stored. In this case, it contains the params.yaml, which contains the execution parameters. You can take a look at it. Each individual parameter of the file has its own description.

### lane_detection
This is the folder in which the executables are stored. Contains a subfolder image_processing, in which all self-created libraries are stored.

### image_lanelet.py
In terms of the idea, the image_lanelet node takes an already adjusted image, which then has to be adjusted so that The lines of the lane are recognised and 2 point lists are created from these lines for the middle and right lane respectively, which are then output using a lanelet_msg. the lanelet_msg contains a header with metadata of the data processing
